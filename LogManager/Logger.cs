﻿using System;
using System.IO;

namespace LogManager
{
    public static class Logger
    {
        private static string _path;
        private static string _fileName;
        private static string _fullPath;

        public static void Info(string text)
        {
            WriteLog("INFO", text);
        }

        public static void Warning(string text)
        {
            WriteLog("WARNING", text);
        }

        public static void Error(string text)
        {
            WriteLog("ERROR", text);
        }

        private static void WriteLog(string lavelLog, string text)
        {
            CheckDirecrories();

            if (string.IsNullOrWhiteSpace(text))
            {
                text = string.Empty;
            }

            text = text.Trim();

            var dateTime = DateTime.Now;
            var lavel = lavelLog.ToUpper().Trim();
            var textLog = dateTime + " | " + lavel + " | " + text;

            using (var appendText = File.AppendText(_fullPath))
            {
                appendText.WriteLine(textLog);
            }
        }

        private static void CheckDirecrories()
        {
            _path = Directory.GetCurrentDirectory() + "/Logs";
            _fileName = "Log-" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".txt";
            _fullPath = Path.Combine(_path, _fileName);

            if (Directory.Exists(_path) == false)
            {
                Directory.CreateDirectory(_path);
            }

            if (File.Exists(_fullPath) == false)
            {
                File.Create(_fullPath).Close();
            }
        }
    }
}
