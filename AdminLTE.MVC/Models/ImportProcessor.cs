﻿using System;
using Data.Repos;
using Data.Entities;
using System.Collections.Generic;
using static AdminLTE.Models.Enums;

namespace AdminLTE.Models
{
    public static class ImportProcessor
    {
        public static List<string> DoProcess(List<string> arr, ExportMode mode)
        {
            if (arr == null || arr.Count == 0)
            {
                return null;
            }

            if (mode == ExportMode.ExportBooks)
            {
                return DoProcessBooks(arr);
            }
            else if (mode == ExportMode.ExportReader)
            {
                return DoProcessReaders(arr);
            }
            else
            {
                return null;
            }
        }

        private static List<string> DoProcessReaders(List<string> arr)
        {
            var retVal = new List<string>();
            var readers = new List<Reader>();
            foreach (var a in arr)
            {
                var splitedArr = a.Split(new string[] { GlobalVariables.ImportSeparatot }, System.StringSplitOptions.None);
                if (splitedArr.Length < 5)
                {
                    continue;
                }

                if (splitedArr[0].IsEmpty() || splitedArr[1].IsEmpty() || splitedArr[2].IsEmpty() || splitedArr[4].IsEmpty())
                {
                    retVal.Add(splitedArr[0]);
                    continue;
                }

                var inn = splitedArr[0].Trim();
                var sName = splitedArr[1].Trim();
                var name = splitedArr[2].Trim();
                var pName = splitedArr[3].Trim();
                var className = splitedArr[4].Trim();

                var hasReader = ReposExtensions.GetEntity<Reader>(x => inn.IsEmpty() == false && x.UniqueId == inn);
                if (hasReader != null)
                {
                    continue;
                }

                var newReader = new Reader
                {
                    UniqueId = inn.IsEmpty() ? null : inn,
                    Sname = sName,
                    Name = name,
                    Pname = pName,
                    ClassId = GetClass(className).Id
                };

                readers.Add(newReader);
            }

            foreach (var reader in readers)
            {
                bool addRes;
                try
                {
                    addRes = ReposExtensions.AddEntity(reader);
                }
                catch
                {
                    addRes = false;
                }

                if (addRes == false)
                {
                    retVal.Add(reader.UniqueId);
                }
            }

            return retVal;
        }

        private static List<string> DoProcessBooks(List<string> arr)
        {
            var retVal = new List<string>();
            var books = new List<BookType>();
            foreach (var a in arr)
            {
                var splitedArr = a.Split(new string[] { GlobalVariables.ImportSeparatot }, System.StringSplitOptions.None);
                if (splitedArr.Length < 9)
                {
                    continue;
                }

                if (splitedArr[0].IsEmpty() || splitedArr[5].IsEmpty() || splitedArr[6].IsEmpty() || splitedArr[7].IsEmpty())
                {
                    retVal.Add(splitedArr[0]);
                    continue;
                }

                var bookName = splitedArr[0].Trim();
                var isbn = splitedArr[1].Trim();
                var seria = splitedArr[2].Trim();
                var year = splitedArr[3].Trim();
                var publisher = splitedArr[4].Trim();
                var bookshelf = splitedArr[5].Trim();
                var author = splitedArr[6].Trim();
                var group = splitedArr[7].Trim();
                var town = splitedArr[8].Trim();

                var hasBook = ReposExtensions.GetEntity<BookType>(x => x.Name == bookName);
                if (hasBook != null)
                {
                    continue;
                }

                var newBook = new BookType
                {
                    Name = bookName,
                    ISBN = isbn,
                    Seria = seria,
                    Year = year.IsEmpty() ? 1900 : Convert.ToInt32(year),
                    Publisher = publisher,
                    BookShelfId = GetBookShelf(bookshelf).Id,
                    AuthorId = GetAuthor(author).Id,
                    BookGroupId = GetBookGroup(group).Id,
                    TownId = GetTown(town).Id
                };

                books.Add(newBook);
            }

            foreach (var book in books)
            {
                bool addRes;
                try
                {
                    addRes = ReposExtensions.AddEntity(book);
                }
                catch
                {
                    addRes = false;
                }

                if (addRes == false)
                {
                    retVal.Add(book.Name);
                }
            }

            return retVal;
        }

        private static Class GetClass(string className)
        {
            if (className.IsEmpty())
            {
                className = "Не указан";
            }

            var cl = ReposExtensions.GetEntity<Class>(x => x.Name == className);
            if (cl == null)
            {
                cl = new Class
                {
                    Name = className
                };
                ReposExtensions.AddEntity(cl);
            }

            return cl;
        }

        private static BookShelf GetBookShelf(string bookshelfName)
        {
            var bookshelf = ReposExtensions.GetEntity<BookShelf>(x => x.Name == bookshelfName);
            if (bookshelf == null)
            {
                bookshelf = new BookShelf
                {
                    Name = bookshelfName
                };
                ReposExtensions.AddEntity(bookshelf);
            }

            return bookshelf;
        }

        private static Author GetAuthor(string authorName)
        {
            var author = ReposExtensions.GetEntity<Author>(x => x.Name == authorName);
            if (author == null)
            {
                author = new Author
                {
                    Name = authorName
                };
                ReposExtensions.AddEntity(author);
            }

            return author;
        }

        private static BookGroup GetBookGroup(string bookGroupName)
        {
            var bookGroup = ReposExtensions.GetEntity<BookGroup>(x => x.Name == bookGroupName);
            if (bookGroup == null)
            {
                bookGroup = new BookGroup
                {
                    Name = bookGroupName
                };
                ReposExtensions.AddEntity(bookGroup);
            }

            return bookGroup;
        }

        private static Town GetTown(string townName)
        {
            if (townName.IsEmpty())
            {
                townName = "Не указан";
            }

            var town = ReposExtensions.GetEntity<Town>(x => x.Name == townName);
            if (town == null)
            {
                town = new Town
                {
                    Name = townName
                };
                ReposExtensions.AddEntity(town);
            }

            return town;
        }
    }
}
