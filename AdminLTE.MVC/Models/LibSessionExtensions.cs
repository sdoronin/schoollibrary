﻿using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace AdminLTE.Models
{
    public static class LibSessionExtensions
    {
        public static void Set(Controller controller, string key, object val)
        {
            var jsonStr = JsonConvert.SerializeObject(val);
            controller.HttpContext.Session.SetString(key, jsonStr);
        }

        public static T Get<T>(Controller controller, string key) where T : class
        {
            if (key.IsEmpty())
            {
                return default(T);
            }

            var sessionValue = controller.HttpContext.Session.GetString(key);
            if (sessionValue.IsEmpty())
            {
                return default(T);
            }

            var obj = JsonConvert.DeserializeObject<T>(sessionValue);
            if (obj == null)
            {
                return default(T);
            }

            if ((obj is T) == false)
            {
                return default(T);
            }

            var retVal = obj as T;
            return retVal;
        }
    }
}
