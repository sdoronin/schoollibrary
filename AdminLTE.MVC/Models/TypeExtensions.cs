﻿using System;

namespace AdminLTE.Models
{
    public static class TypeExtensions
    {
        public static bool ToBool(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return false;
            }

            source = source.Trim();
            bool hasChanges;
            var parseHasChange = bool.TryParse(source, out hasChanges);
            return parseHasChange == false ? false : hasChanges;
        }

        public static long ToLong(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return 0;
            }

            source = source.Trim();

            long parsedLong;
            var isParse = long.TryParse(source, out parsedLong);
            return isParse == false ? 0 : parsedLong;
        }

        public static int ToInt(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return 0;
            }

            source = source.Trim();

            int parsedInt;
            var isParse = int.TryParse(source, out parsedInt);
            return isParse == false ? 0 : parsedInt;
        }

        public static DateTime ToStartDate(this DateTime source)
        {
            return source.Date;
        }

        public static DateTime ToEndDate(this DateTime source)
        {
            return new DateTime(source.Year, source.Month, source.Day, 23, 59, 59);
        }

        public static bool IsEmpty(this string source)
        {
            return string.IsNullOrWhiteSpace(source);
        }
    }
}
