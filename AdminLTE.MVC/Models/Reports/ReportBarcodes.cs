using System.Linq;

namespace AdminLTE.Models.Reports
{
    public partial class ReportBarcodes
    {
        public ReportBarcodes(int barcode, string bookName)
        {
            InitializeComponent();
            this.barCode1.Text = barcode.ToString();
            if (string.IsNullOrWhiteSpace(bookName) == false)
            {
                this.lblBookName.Visible = true;
                this.lblBookName.Text = bookName.Trim();
            }
        }
    }
}
