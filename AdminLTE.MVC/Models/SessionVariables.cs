﻿using System;

namespace AdminLTE.Models
{
    public static class SessionVariables
    {
        public const string FirstPrintReportInCurrentPage = "FirstPrintReportInCurrentPage";
    }
}
