﻿using System;

namespace AdminLTE.ViewModels
{
    public class LendingViewModel
    {
        public int ReaderId { get; set; }
        public string Barcodes { get; set; }
    }
}
