﻿using Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace AdminLTE.ViewModels
{
    public class ReaderViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Фамилия")]
        public string Sname { get; set; }

        [Display(Name = "Отчество")]
        public string Pname { get; set; }

        public int ClassId { get; set; }

        public string ClassName { get; set; }

        public string UniqueId { get; set; }

        public static implicit operator ReaderViewModel(Reader r)
        {
            if (r == null)
            {
                return new ReaderViewModel();
            }

            return new ReaderViewModel
            {
                Id = r.Id,
                Name = r.Name,
                Pname = r.Pname,
                Sname = r.Sname,
                ClassId = r.ClassId,
                UniqueId = r.UniqueId,
                ClassName = r.Class.Name
            };
        }
    }
}
