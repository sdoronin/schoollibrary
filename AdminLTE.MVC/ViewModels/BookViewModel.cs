﻿using Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace AdminLTE.ViewModels
{
    public class BookViewModel
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int GroupId { get; set; }
        public int BookshelfId { get; set; }
        public int? TownId { get; set; }

        [Display(Name = "Год")]
        public int Year { get; set; }

        [Display(Name = "ISBN")]
        public string ISBN { get; set; }

        [Display(Name = "Серия")]
        public string Seria { get; set; }

        [Display(Name = "Издатель")]
        public string Publisher { get; set; }

        public string AuthorName { get; set; }
        public string GroupName { get; set; }
        public string BookshelfName { get; set; }
        public string TownName { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }

        public string FullName
        {
            get
            {
                return this.Name + ", (" + this.AuthorName + "), " + "г: " + this.Year;
            }
        }

        public static implicit operator BookViewModel(BookType b)
        {
            if (b == null)
            {
                return new BookViewModel();
            }

            return new BookViewModel
            {
                Id = b.Id,
                AuthorId = b.AuthorId,
                GroupId = b.BookGroupId,
                ISBN = b.ISBN,
                Seria = b.Seria,
                Year = b.Year,
                BookshelfId = b.BookShelfId,
                AuthorName = b.Author.Name,
                GroupName = b.BookGroup.Name,
                BookshelfName = b.BookShelf.Name,
                Name = b.Name,
                TownId = b.TownId,
                TownName = b.Town == null ? string.Empty : b.Town.Name,
                Publisher = b.Publisher
            };
        }
    }
}
