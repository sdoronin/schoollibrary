﻿using Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace AdminLTE.ViewModels
{
    public class BookshelfViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        public static implicit operator BookshelfViewModel(BookShelf b)
        {
            if (b == null)
            {
                return new BookshelfViewModel();
            }

            return new BookshelfViewModel
            {
                Id = b.Id,
                Name = b.Name
            };
        }
    }
}
