﻿using Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace AdminLTE.ViewModels
{
    public class AuthorViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Автор")]
        public string Name { get; set; }

        public static implicit operator AuthorViewModel(Author a)
        {
            if (a == null)
            {
                return new AuthorViewModel();
            }

            return new AuthorViewModel
            {
                Id = a.Id,
                Name = a.Name
            };
        }
    }
}
