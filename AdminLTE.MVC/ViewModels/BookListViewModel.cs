﻿using Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace AdminLTE.ViewModels
{
    public class BookListViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Штрихкод")]
        public string Barcode { get; set; }
        public int BookTypeId { get; set; }
        public BookViewModel BookModel { get; set; }

        public static implicit operator BookListViewModel(Book b)
        {
            if (b == null)
            {
                return new BookListViewModel();
            }

            return new BookListViewModel
            {
                Id = b.Id,
                Barcode = b.Barcode,
                BookModel = new BookViewModel
                {
                    Id = b.BookTypeId,
                    AuthorId = b.BookType.AuthorId,
                    AuthorName = b.BookType.Author.Name,
                    BookshelfId = b.BookType.BookShelfId,
                    BookshelfName = b.BookType.BookShelf.Name,
                    GroupId = b.BookType.BookGroupId,
                    GroupName = b.BookType.BookGroup.Name,
                    ISBN = b.BookType.ISBN,
                    Seria = b.BookType.Seria,
                    Year = b.BookType.Year,
                    Name = b.BookType.Name
                },
                BookTypeId = b.BookTypeId
            };
        }
    }
}
