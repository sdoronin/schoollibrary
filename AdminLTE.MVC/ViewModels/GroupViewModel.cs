﻿using Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace AdminLTE.ViewModels
{
    public class GroupViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        public static implicit operator GroupViewModel(BookGroup g)
        {
            if (g == null)
            {
                return new GroupViewModel();
            }

            return new GroupViewModel
            {
                Id = g.Id,
                Name = g.Name
            };
        }
    }
}
