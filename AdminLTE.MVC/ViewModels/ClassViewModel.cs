﻿using Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace AdminLTE.ViewModels
{
    public class ClassViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        public static implicit operator ClassViewModel(Class c)
        {
            if (c == null)
            {
                return new ClassViewModel();
            }

            return new ClassViewModel
            {
                Id = c.Id,
                Name = c.Name
            };
        }
    }
}
