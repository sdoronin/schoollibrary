﻿using System;
using Data.Entities;

namespace AdminLTE.ViewModels
{
    public class RegisterViewModel
    {
        public int ReaderId { get; set; }
        public int BookTypeId { get; set; }
        public string Barcode { get; set; }
        public int DaysLeft { get; set; }
        public bool OnlyOverdue { get; set; }
        public DateTime DateIssued { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public string ReaderName { get; set; }
        public string BootTypeName { get; set; }

        public static implicit operator RegisterViewModel(IssuedBook i)
        {
            if (i == null)
            {
                return new RegisterViewModel();
            }

            var retVal = new RegisterViewModel
            {
                Barcode = i.Book.Barcode,
                ReaderId = i.ReaderId,
                BookTypeId = i.Book.BookTypeId,
                ReaderName = i.Reader.FullName,
                BootTypeName = i.Book.BookType.Name,
                DateIssued = i.DateIssue
            };

            if (i.DayCount == 0)
            {
                retVal.DaysLeft = 0;
            }
            else
            {
                var dateNow = DateTime.Now;
                var endDate = i.DateIssue.AddDays(i.DayCount);
                var span = dateNow - endDate;
                var days = Convert.ToInt32(span.TotalDays);

                retVal.DaysLeft = days;
            }

            return retVal;
        }
    }
}
