﻿using DevExpress.XtraReports.UI;

namespace AdminLTE.ViewModels
{
    public class ReportViewModel
    {
        public XtraReport Report { get; set; }
        public string ReportHeight { get; set; }
        public string ReportWidth { get; set; }
    }
}
