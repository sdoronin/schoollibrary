﻿using System.Collections.Generic;

namespace AdminLTE.ViewModels
{
    public class ImportBooksViewModel
    {
        public string Error { get; set; }
        public List<string> Warnings { get; set; }

        public bool IsSuccess
        {
            get
            {
                return this.Warnings == null || this.Warnings.Count == 0;
            }
        }
    }
}
