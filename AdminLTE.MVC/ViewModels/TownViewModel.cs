﻿using Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace AdminLTE.ViewModels
{
    public class TownViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }

        public static implicit operator TownViewModel(Town t)
        {
            if (t == null)
            {
                return new TownViewModel();
            }

            return new TownViewModel
            {
                Id = t.Id,
                Name = t.Name
            };
        }
    }
}
