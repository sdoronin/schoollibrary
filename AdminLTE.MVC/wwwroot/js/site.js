﻿function UpperFirstChar(element) {
    var val = element.value.trim();
    if (val == null) {
        element.value = "";
        return;
    }
    
    var txt = val.charAt(0).toUpperCase() + val.slice(1);
    element.value = txt.trim();
}

function OnchangeValue() {
    document.getElementById('HasChanged').value = true;
}

function SaveAuthor() {
    var hasChanged = document.getElementById('HasChanged').value;
    if (hasChanged == "true") {
        return false;
    }

    var name = document.getElementById('txtName').value;
    if (name.trim() == "") {
        alert("Наименование не может быть пустым");
        return false;
    }

    var id = document.getElementById('txtAuthorId').value;

    var data = {
        Id: id,
        Name: name
    };

    $.ajax({
        type: 'POST',
        url: '/Author/Edit',
        data: data,

        success: function (result) {
            $('#divAuthorsContent').html(result);
            return true;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}

function SaveGroup() {
    var hasChanged = document.getElementById('HasChanged').value;
    if (hasChanged == "true") {
        return false;
    }

    var name = document.getElementById('txtName').value;
    if (name.trim() == "") {
        alert("Наименование не может быть пустым");
        return false;
    }

    var id = document.getElementById('txtGroupId').value;

    var data = {
        Id: id,
        Name: name
    };

    $.ajax({
        type: 'POST',
        url: '/Group/Edit',
        data: data,

        success: function (result) {
            $('#divGroupsContent').html(result);
            return true;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}

function SaveBookshelf() {
    var hasChanged = document.getElementById('HasChanged').value;
    if (hasChanged == "true") {
        return false;
    }

    var name = document.getElementById('txtName').value;
    if (name.trim() == "") {
        alert("Наименование не может быть пустым");
        return false;
    }

    var id = document.getElementById('txtBookshelfId').value;

    var data = {
        Id: id,
        Name: name
    };

    $.ajax({
        type: 'POST',
        url: '/Bookshelf/Edit',
        data: data,

        success: function (result) {
            $('#divBookshelfContent').html(result);
            return true;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}

function SaveBook() {

    var id = document.getElementById('txtBookId').value;
    var isbn = document.getElementById('txtIsbn').value;
    var seria = document.getElementById('txtSeria').value;
    var year = document.getElementById('txtYear').value;
    var publisher = document.getElementById('txtPublisher').value;
    var name = document.getElementById('txtBootTypeName').value;
    var authorId = document.getElementById("selectedAuthor")
        .options[document.getElementById("selectedAuthor").selectedIndex].value;
    var groupId = document.getElementById("selectedGroup")
        .options[document.getElementById("selectedGroup").selectedIndex].value;
    var bookshelf = document.getElementById("selectedBookshel")
        .options[document.getElementById("selectedBookshel").selectedIndex].value;
    var townId = document.getElementById("selectedTown")
        .options[document.getElementById("selectedTown").selectedIndex].value;

    var data = {
        Id: id,
        AuthorId: authorId,
        GroupId: groupId,
        Year: year,
        ISBN: isbn,
        Seria: seria,
        BookshelfId: bookshelf,
        Name: name,
        TownId: townId,
        Publisher: publisher
    };

    $.ajax({
        type: 'POST',
        url: '/Book/Edit',
        data: data,

        success: function (result) {
            $('#divBookItemAddContent').html(result);
            return true;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}

function SaveClass() {
    var hasChanged = document.getElementById('HasChanged').value;
    if (hasChanged == "true") {
        return false;
    }

    var name = document.getElementById('txtClassName').value;
    if (name.trim() == "") {
        alert("Наименование не может быть пустым");
        return false;
    }

    var id = document.getElementById('txtClassId').value;

    var data = {
        Id: id,
        Name: name
    };

    $.ajax({
        type: 'POST',
        url: '/Class/Edit',
        data: data,

        success: function (result) {
            $('#divClassContent').html(result);
            return true;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}

function SaveReader() {
    var hasChanged = document.getElementById('HasChanged').value;
    if (hasChanged == "false") {
        return false;
    }

    var name = document.getElementById('txtName').value;
    if (name.trim() == "") {
        alert("Имя не может быть пустым");
        return false;
    }

    var sname = document.getElementById('txtSname').value;
    if (sname.trim() == "") {
        alert("Фамилия не может быть пустым");
        return false;
    }

    var pname = document.getElementById('txtPname').value;

    var classId = document.getElementById("selectedClass")
        .options[document.getElementById("selectedClass").selectedIndex].value;
    if (classId == "" || classId == "") {
        alert("Укажите класс");
        return false;
    }

    var id = document.getElementById('txtReaderId').value;
    var inn = document.getElementById('txtInn').value;

    var data = {
        Id: id,
        Name: name,
        Sname: sname,
        Pname: pname,
        ClassId: classId,
        UniqueId: inn
    };

    $.ajax({
        type: 'POST',
        url: '/Reader/Edit',
        data: data,

        success: function (result) {
            $('#divReaderContent').html(result);
            return true;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}

function GetLeadingPartial(allBarcodes) {
    var readerId = document.getElementById("selectReader")
        .options[document.getElementById("selectReader").selectedIndex].value.trim();
    if (readerId == "") {
        alert("Выбирите читателя");
        return false;
    }

    allBarcodes = allBarcodes.trim();
    if (allBarcodes == "") {
        alert("Не указан ни один штрихкод");
        return false;
    }

    var barcode = document.getElementById("txtBarcode").value;

    var data = {
        barcodes: allBarcodes,
        barcode: barcode,
        readerId: readerId
    };

    $.ajax({
        type: 'POST',
        url: '/Lending/GetPartialContent',
        data: data,

        success: function (result) {
            $('#divLeadingContent').html(result);
            return false;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
            return false;
        },

        error: function (response) {
            alert("error" + response.responseText);
            return false;
        }
    });
}

function SaveBookList() {
    var id = document.getElementById('txtBookId').value;
    var barcode = document.getElementById('txtBookBarcode').value;

    var typeId = document.getElementById("selectedBookType")
        .options[document.getElementById("selectedBookType").selectedIndex].value;

    var data = {
        Id: id,
        Barcode: barcode,
        BookTypeId: typeId
    };

    $.ajax({
        type: 'POST',
        url: '/BookList/Edit',
        data: data,

        success: function (result) {
            $('#divBooksContent').html(result);
            return true;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}

function GetReturnBookPartial(allBarcodes) {
    var readerId = document.getElementById("selectReader")
        .options[document.getElementById("selectReader").selectedIndex].value.trim();
    if (readerId == "") {
        alert("Выбирите читателя");
        return false;
    }

    allBarcodes = allBarcodes.trim();
    if (allBarcodes == "") {
        alert("Не указан ни один штрихкод");
        return false;
    }

    var barcode = document.getElementById("txtBarcode").value;

    var data = {
        barcodes: allBarcodes,
        barcode: barcode,
        readerId: readerId
    };

    $.ajax({
        type: 'POST',
        url: '/ReturnBook/GetPartialContent',
        data: data,

        success: function (result) {
            $('#divLeadingContent').html(result);
            return false;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
            return false;
        },

        error: function (response) {
            alert("error" + response.responseText);
            return false;
        }
    });
}


function PrintBarcodes() {
    var count = document.getElementById("txtCountBarcodes").value.trim();

    if (count == "") {
        alert("Укажите кол-во баркодов для печати");
        return false;
    }

    var bookId = document.getElementById("selectedBookTypeInd")
        .options[document.getElementById("selectedBookTypeInd").selectedIndex].value.trim();
    var prit_doubles = $("#chk_print_doubles").is(":checked");

    var data = { count: count, bookId: bookId, print_doubles: prit_doubles };

    $.ajax({
        type: 'GET',
        url: '/Report/GetReport',
        data: data,

        success: function (result) {
            $('#detailPrint').html(result);
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}

function GetRegisterContent() {
    var readerId = document.getElementById("selectReader")
        .options[document.getElementById("selectReader").selectedIndex].value;

    var barcod = document.getElementById("txtBarcode").value.trim();
    var only_overdue = $("#chkOnlyOverdue").is(":checked");
    var dt_from = document.getElementById("dt_from").value;
    var dt_to = document.getElementById("dt_to").value;

    var data = {
        ReaderId: readerId,
        Barcode: barcod,
        OnlyOverdue: only_overdue,
        DateFrom: dt_from,
        DateTo: dt_to
    };

    $.ajax({
        type: 'POST',
        url: '/Register/GetContent',
        data: data,

        success: function (result) {
            $('#divRegisterContent').html(result);
            return true;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}

function SaveTown() {
    var hasChanged = document.getElementById('HasChanged').value;
    if (hasChanged == "true") {
        return false;
    }

    var name = document.getElementById('txtName').value;
    if (name.trim() == "") {
        alert("Наименование не может быть пустым");
        return false;
    }

    var id = document.getElementById('txtTownId').value;

    var data = {
        Id: id,
        Name: name
    };

    $.ajax({
        type: 'POST',
        url: '/Towns/Edit',
        data: data,

        success: function (result) {
            $('#divTownsContent').html(result);
            return true;
        },

        failure: function (response) {
            alert("failure" + response.responseText);
        },

        error: function (response) {
            alert("error" + response.responseText);
        }
    });
}
