﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AdminLTE.Migrations
{
    public partial class AddTownTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Publisher",
                table: "BookTypes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TownId",
                table: "BookTypes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Towns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Towns", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BookTypes_TownId",
                table: "BookTypes",
                column: "TownId");

            migrationBuilder.CreateIndex(
                name: "IX_Towns_Id",
                table: "Towns",
                column: "Id",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BookTypes_Towns_TownId",
                table: "BookTypes",
                column: "TownId",
                principalTable: "Towns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookTypes_Towns_TownId",
                table: "BookTypes");

            migrationBuilder.DropTable(
                name: "Towns");

            migrationBuilder.DropIndex(
                name: "IX_BookTypes_TownId",
                table: "BookTypes");

            migrationBuilder.DropColumn(
                name: "Publisher",
                table: "BookTypes");

            migrationBuilder.DropColumn(
                name: "TownId",
                table: "BookTypes");
        }
    }
}
