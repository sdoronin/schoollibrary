using System.IO;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

// - Usings for service mode
using System.Linq;
using System.Diagnostics;
using Microsoft.AspNetCore.Hosting.WindowsServices;

namespace AdminLTE.MVC
{
    public class Program
    {
        public static void Main(string[] args)
        {
#if (DEBUG)
            CreateHostBuilder(args).Build().Run();
#else
            var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
            var pathToContentRoot = Path.GetDirectoryName(pathToExe);
            Directory.SetCurrentDirectory(pathToContentRoot);
            var builder = CreateWebHostBuilderForService(args.Where(arg => arg != "--console").ToArray());
            var host = builder.Build();
            host.RunAsService();
#endif
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseKestrel();
                    webBuilder.UseIIS();
                    webBuilder.UseIISIntegration();
                    webBuilder.ConfigureKestrel(serverOptions =>
                    {
                        serverOptions.Limits.MaxRequestBodySize = 100000000;
                    })
                    .UseStartup<Startup>();
                });

        public static IWebHostBuilder CreateWebHostBuilderForService(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("config.json", optional: true, reloadOnChange: true)
                .Build();
            var urls = config.GetValue<string>("url", "http://*:5002/");
            return WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .UseUrls(urls)
                .UseStartup<Startup>();
        }
    }
}
