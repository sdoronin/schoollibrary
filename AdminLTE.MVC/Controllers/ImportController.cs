﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AdminLTE.Models;
using AdminLTE.ViewModels;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class ImportController : Controller
    {
        private string _currentPath;
        public ImportController()
        {
            _currentPath = Directory.GetCurrentDirectory();
        }

        public IActionResult ImportReaders()
        {
            return View("ImportReaders");
        }

        public IActionResult ImportBooks()
        {
            return View("ImportBooks");
        }

        public IActionResult DownloadImportBooksTemplate()
        {
            string path = Path.Combine(_currentPath, "ImportTemplates/ImportBooksTemplate.xlsx");
            byte[] mas = System.IO.File.ReadAllBytes(path);
            string file_type = "application/vnd.ms-excel";
            string file_name = "ImportBooksTemplate.xlsx";
            return File(mas, file_type, file_name);
        }

        public IActionResult DownloadImportReadersTemplate()
        {
            string path = Path.Combine(_currentPath, "ImportTemplates/ImportReadersTemplate.xlsx");
            byte[] mas = System.IO.File.ReadAllBytes(path);
            string file_type = "application/vnd.ms-excel";
            string file_name = "ImportBooksTemplate.xlsx";
            return File(mas, file_type, file_name);
        }

        [HttpPost]
        public async Task<ActionResult> UploadBooks(IList<IFormFile> files)
        {
            var model = await ProcessFile(files, Enums.ExportMode.ExportBooks);
            return PartialView("_ImportBooksResult", model);
        }

        [HttpPost]
        public async Task<ActionResult> UploadReaders(IList<IFormFile> files)
        {
            var model = await ProcessFile(files, Enums.ExportMode.ExportReader);
            return PartialView("_ImportReadersResult", model);
        }

        private async Task<ImportBooksViewModel> ProcessFile(IList<IFormFile> files, Enums.ExportMode mode)
        {
            var model = new ImportBooksViewModel();
            if (files == null || files.Count == 0)
            {
                model.Error = "С клиента не пришли файлы";
            }

            var file = files[0];
            string filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            filename = this.EnsureCorrectFilename(filename);
            var path = this.GetPathAndFilename(filename);
            using (FileStream output = System.IO.File.Create(path))
            {
                await file.CopyToAsync(output);
            }

            var arr = GetFileLines(path);
            model.Warnings = ImportProcessor.DoProcess(arr, mode);
            return model;
        }

        private List<string> GetFileLines(string path)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            var fileLines = new List<string>();
            using (var stream = System.IO.File.Open(path, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var fileLine = string.Empty;
                            for (int column = 0; column < reader.FieldCount; column++)
                            {
                                fileLine += reader.GetValue(column) + GlobalVariables.ImportSeparatot;
                            }

                            fileLines.Add(fileLine);
                        }
                    } while (reader.NextResult());

                    fileLines.RemoveAt(0);
                }
            }

            return fileLines;
        }

        private string EnsureCorrectFilename(string filename)
        {
            if (filename.Contains("\\"))
            {
                filename = filename.Substring(filename.LastIndexOf("\\") + 1);
            }

            return filename;
        }

        private string GetPathAndFilename(string filename)
        {
            var dir = _currentPath + "\\uploads";
            if (Directory.Exists(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }

            return dir + "\\" + filename;
        }
    }
}
