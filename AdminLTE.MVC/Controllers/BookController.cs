﻿using System.Collections.Generic;
using System.Linq;
using AdminLTE.ViewModels;
using Data.Entities;
using Data.Repos;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class BookController : Controller
    {
        public IActionResult Index()
        {
            return View(GetAllBooks());
        }

        public IActionResult Edit(int id)
        {
            ViewBag.Authors = ReposExtensions.GetEntities<Author>()
                .OrderBy(x => x.Name).Select(x => (AuthorViewModel)x).ToList();

            ViewBag.Groups = ReposExtensions.GetEntities<BookGroup>()
                .OrderBy(x => x.Name).Select(x => (GroupViewModel)x).ToList();

            ViewBag.Bookshelfs = ReposExtensions.GetEntities<BookShelf>()
                .OrderBy(x => x.Name).Select(x => (BookshelfViewModel)x).ToList();

            ViewBag.Towns = ReposExtensions.GetEntities<Town>()
                .OrderBy(x => x.Name).Select(x => (TownViewModel)x).ToList();

            var book = (BookViewModel)ReposExtensions.GetEntity<BookType>(x => x.Id == id);
            return PartialView("_Edit", book);
        }

        [HttpPost]
        public IActionResult Edit(BookViewModel model)
        {
            BookType book;
            if (model.Id > 0)
            {
                book = ReposExtensions.GetEntity<BookType>(x => x.Id == model.Id);
            }
            else
            {
                book = new BookType();
            }

            book.Author = null;
            book.BookGroup = null;
            book.BookShelf = null;
            book.Town = null;

            book.Seria = model.Seria;
            book.ISBN = model.ISBN;
            book.AuthorId = model.AuthorId;
            book.BookGroupId = model.GroupId;
            book.BookShelfId = model.BookshelfId;
            book.Name = model.Name;
            book.Year = model.Year;
            book.TownId = model.TownId;
            book.Publisher = model.Publisher;

            var saveResult = ReposExtensions.AddOrUpdate(book);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!";
            }

            var retModel = GetAllBooks();
            return PartialView("_BooksContent", retModel);
        }

        private IEnumerable<BookViewModel> GetAllBooks()
        {
            var retVal = ReposExtensions.GetEntities<BookType>()
                .OrderBy(x => x.Id).Select(x => (BookViewModel)x).ToArray();
            return retVal;
        }
    }
}
