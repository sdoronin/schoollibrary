﻿using System;
using Data.Repos;
using System.Linq;
using LibData.Repos;
using Data.Entities;
using AdminLTE.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class LendingController : Controller
    {
        private BookListRepository<Book> _bookListRepos;
        public LendingController()
        {
            _bookListRepos = new BookListRepository<Book>();
        }

        public IActionResult Index()
        {
            ViewBag.Readers = ReposExtensions.GetEntities<Reader>()
                .OrderBy(x => x.Name).Select(x => (ReaderViewModel)x).ToList();

            return View();
        }

        [HttpPost]
        public IActionResult Index(LendingViewModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.Barcodes) || model.ReaderId <= 0)
            {
                return View();
            }

            var barcodesArr = model.Barcodes.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var books = _bookListRepos.GetBooksByBarcodes(barcodesArr);
            if (books.Length == 0)
            {
                ViewBag.ErrorMessage = "Не найдены книги";
            }

            var repo = new IssuedBookRepository<IssuedBook>();
            var saveResult = repo.CreateIssuesByBooks(books, model.ReaderId, barcodesArr);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!!!";
            }
            else
            {
                ViewBag.ResultMessage = "Книги выданы";
            }

            return View();
        }

        [HttpPost]
        public IActionResult GetPartialContent(string barcodes, string barcode, int readerId)
        {
            barcode = barcode.Trim();
            var barcodesArr = barcodes.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var checkBookFromReader = _bookListRepos.CheckBookFromReaderForLeading(barcode, readerId, barcodesArr);
            if (string.IsNullOrWhiteSpace(checkBookFromReader) == false)
            {
                barcodes = barcodes.Replace(barcode, "");
                barcodesArr = barcodes.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                ViewBag.ErrorMessage = checkBookFromReader;
            }

            var bookList = _bookListRepos.GetBooksByBarcodes(barcodesArr).Select(x => (BookListViewModel)x).ToArray();
            return PartialView("_LeadingContent", bookList);
        }
    }
}
