﻿using System.Collections.Generic;
using System.Linq;
using AdminLTE.ViewModels;
using Data.Entities;
using Data.Repos;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class GroupController : Controller
    {
        public IActionResult Index()
        {
            return View(GetAllGroups());
        }

        public IActionResult Edit(int id)
        {
            var group = (GroupViewModel)ReposExtensions.GetEntity<BookGroup>(x => x.Id == id);
            return PartialView("_Edit", group);
        }

        [HttpPost]
        public IActionResult Edit(GroupViewModel model)
        {
            BookGroup group;
            if (model.Id > 0)
            {
                group = ReposExtensions.GetEntity<BookGroup>(x => x.Id == model.Id);
            }
            else
            {
                group = new BookGroup();
            }

            group.Name = model.Name.Trim();
            var saveResult = ReposExtensions.AddOrUpdate(group);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!";
            }

            return PartialView("_GroupsContent", GetAllGroups());
        }

        private IEnumerable<GroupViewModel> GetAllGroups()
        {
            var retVal = ReposExtensions.GetEntities<BookGroup>()
                .Select(x => (GroupViewModel)x).OrderBy(x => x.Name).ToArray();
            return retVal;
        }
    }
}
