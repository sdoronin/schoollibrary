﻿using System.Collections.Generic;
using System.Linq;
using AdminLTE.ViewModels;
using Data.Entities;
using Data.Repos;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class AuthorController : Controller
    {
        public IActionResult Index()
        {
            return View(GetAllAuthors());
        }

        public IActionResult Edit(int id)
        {
            var author = (AuthorViewModel)ReposExtensions.GetEntity<Author>(x => x.Id == id);
            return PartialView("_Edit", author);
        }

        [HttpPost]
        public IActionResult Edit(AuthorViewModel model)
        {
            Author author;
            if (model.Id > 0)
            {
                author = ReposExtensions.GetEntity<Author>(x => x.Id == model.Id);
            }
            else
            {
                author = new Author();
            }

            author.Name = model.Name.Trim();
            var saveResult = ReposExtensions.AddOrUpdate(author);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!";
            }

            return PartialView("_AuthorsContent", GetAllAuthors());
        }

        private IEnumerable<AuthorViewModel> GetAllAuthors()
        {
            var retVal = ReposExtensions.GetEntities<Author>()
                .Select(x => (AuthorViewModel)x).OrderBy(x => x.Name).ToArray();
            return retVal;
        }
    }
}
