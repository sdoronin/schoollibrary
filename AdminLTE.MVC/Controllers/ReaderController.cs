﻿using System.Collections.Generic;
using System.Linq;
using AdminLTE.ViewModels;
using Data.Entities;
using Data.Repos;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class ReaderController : Controller
    {
        public IActionResult Index()
        {
            return View(GetAllReaders());
        }

        public IActionResult Edit(int id)
        {
            ViewBag.Classes = ReposExtensions.GetEntities<Class>().Select(x => (ClassViewModel)x).ToList();

            var reader = (ReaderViewModel)ReposExtensions.GetEntity<Reader>(x => x.Id == id);
            return PartialView("_Edit", reader);
        }

        [HttpPost]
        public IActionResult Edit(ReaderViewModel model)
        {
            Reader reader;
            if (model.Id > 0)
            {
                reader = ReposExtensions.GetEntity<Reader>(x => x.Id == model.Id);
            }
            else
            {
                reader = new Reader();
            }

            string inn = model.UniqueId == null ? null : model.UniqueId.Trim();
            var hasUnique = ReposExtensions.GetEntity<Reader>(x => x.UniqueId == inn);
            if (hasUnique != null && hasUnique.Id != reader.Id && inn != null)
            {
                ViewBag.ErrorMessage = "Читатель с таким ИНН уже есть в базе";
                return PartialView("_ReaderContent", GetAllReaders());
            }

            reader.ClassId = model.ClassId;
            reader.Name = model.Name.Trim();
            reader.Sname = model.Sname;
            reader.Pname = model.Pname;
            reader.UniqueId = inn;

            var saveResult = ReposExtensions.AddOrUpdate(reader);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!";
            }

            return PartialView("_ReaderContent", GetAllReaders());
        }

        [HttpPost]
        public IActionResult Search(string filter_text)
        {
            var allreaders = GetAllReaders();
            if (string.IsNullOrWhiteSpace(filter_text))
            {
                return PartialView("_ReaderContent", allreaders);
            }

            filter_text = filter_text.Trim();

            var isInn = filter_text.All(char.IsDigit);
            if (isInn)
            {
                allreaders = allreaders.Where(x => x.UniqueId == filter_text).ToArray();
                return PartialView("_ReaderContent", allreaders);
            }

            var arr = filter_text.Split(new string[] { " ", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);
            if (arr.Length == 1)
            {
                allreaders = allreaders.Where(x =>
                        x.Name.ToLower().Contains(arr[0].ToLower())
                        || (x.Pname != null && x.Pname.ToLower().Contains(arr[0].ToLower()))
                        || (x.Sname != null && x.Sname.ToLower().Contains(arr[0].ToLower())))
                    .ToArray();
                return PartialView("_ReaderContent", allreaders);
            }
            else if (arr.Length == 2)
            {
                allreaders = allreaders.Where(x =>
                        x.Sname.ToLower().Contains(arr[0].ToLower())
                        && x.Name.ToLower().Contains(arr[1].ToLower()))
                    .ToArray();
                return PartialView("_ReaderContent", allreaders);
            }
            else if (arr.Length == 3)
            {
                allreaders = allreaders.Where(x =>
                        x.Sname.ToLower().Contains(arr[0].ToLower())
                        && x.Name.ToLower().Contains(arr[1].ToLower())
                        && x.Pname.ToLower().Contains(arr[2].ToLower()))
                    .ToArray();
                return PartialView("_ReaderContent", allreaders);
            }
            else
            {
                return PartialView("_ReaderContent", allreaders);
            }
        }

        private IEnumerable<ReaderViewModel> GetAllReaders()
        {
            var retVal = ReposExtensions.GetEntities<Reader>()
                .Select(x => (ReaderViewModel)x).OrderBy(x => x.Sname).ToArray();
            return retVal;
        }
    }
}
