﻿using System;
using Data.Repos;
using System.Linq;
using LibData.Repos;
using Data.Entities;
using AdminLTE.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class ReturnBookController : Controller
    {
        private BookListRepository<Book> _bookListRepos;
        public ReturnBookController()
        {
            _bookListRepos = new BookListRepository<Book>();
        }

        public IActionResult Index()
        {
            ViewBag.Readers = ReposExtensions.GetEntities<Reader>()
                .OrderBy(x => x.Name).Select(x => (ReaderViewModel)x).ToList();

            return View();
        }

        [HttpPost]
        public IActionResult Index(ReturnBookViewModel model)
        {
            var barcodesArr = model.Barcodes.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var books = _bookListRepos.GetBooksByBarcodes(barcodesArr);
            var repo = new IssuedBookRepository<IssuedBook>();
            var saveResult = repo.ReturnBooks(books, model.ReaderId);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!!!";
            }
            else
            {
                ViewBag.ResultMessage = "Возврат произведен";
            }

            return View();
        }

        [HttpPost]
        public IActionResult GetPartialContent(string barcodes, string barcode, int readerId)
        {
            barcode = barcode.Trim();
            var barcodesArr = barcodes.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var checkBookFromReader = _bookListRepos.CheckBookFromReaderForReturn(barcode, readerId);
            if (string.IsNullOrWhiteSpace(checkBookFromReader) == false)
            {
                barcodes = barcodes.Replace(barcode, "");
                barcodesArr = barcodes.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                ViewBag.ErrorMessage = checkBookFromReader;
            }

            var bookList = _bookListRepos.GetBooksByBarcodes(barcodesArr).Select(x => (BookListViewModel)x).ToArray();
            return PartialView("_ReturnBookContent", bookList);
        }
    }
}
