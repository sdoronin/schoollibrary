﻿using System.Collections.Generic;
using System.Linq;
using AdminLTE.Models;
using AdminLTE.ViewModels;
using Data.Entities;
using Data.Repos;
using LibData.Repos;
using Microsoft.AspNetCore.Mvc;
using static LibData.Enums;

namespace AdminLTE.Controllers
{
    public class BookListController : Controller
    {
        private BookListRepository<Book> _bookListRepos;
        public BookListController()
        {
            _bookListRepos = new BookListRepository<Book>();
        }

        public IActionResult Index()
        {
            LibSessionExtensions.Set(this, SessionVariables.FirstPrintReportInCurrentPage, "1");
            ViewBag.BookTypes = ReposExtensions.GetEntities<BookType>().Select(x => (BookViewModel)x).ToList();
            return View(GetAllBooks());
        }

        public IActionResult Edit(int id)
        {
            var book = (BookListViewModel)_bookListRepos.GetBook(id);
            ViewBag.BookTypes = ReposExtensions.GetEntities<BookType>().Select(x => (BookViewModel)x).ToList();
            return PartialView("_Edit", book);
        }

        [HttpPost]
        public IActionResult AddNewBook(BookListViewModel model)
        {
            var book = new Book
            {
                Barcode = model.Barcode.Trim(),
                BookTypeId = model.BookTypeId
            };

            var res = _bookListRepos.AddNewBook(book);
            if (string.IsNullOrWhiteSpace(res) == false)
            {
                ViewBag.ErrorMessage = res;
            }

            return PartialView("_BookListContent", GetAllBooks(20, OrderBy.Desc));
        }

        [HttpPost]
        public IActionResult Edit(BookListViewModel model)
        {
            if(string.IsNullOrWhiteSpace(model.Barcode))
            {
                ViewBag.ErrorMessage = "Не указан штрихкод книги!";
                return PartialView("_BookListContent", GetAllBooks());
            }

            Book book;
            if (model.Id > 0)
            {
                book = ReposExtensions.GetEntity<Book>(x => x.Id == model.Id);
            }
            else
            {
                book = new Book();
            }

            book.Barcode = model.Barcode.Trim();
            book.BookTypeId = model.BookTypeId;

            var hasBookMessage = _bookListRepos.HasBook(book);
            if (string.IsNullOrWhiteSpace(hasBookMessage) == false)
            {
                ViewBag.ErrorMessage = hasBookMessage;
                return PartialView("_BookListContent", GetAllBooks());
            }

            var saveResult = ReposExtensions.AddOrUpdate(book);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!";
            }

            return PartialView("_BookListContent", GetAllBooks());
        }

        private IEnumerable<BookListViewModel> GetAllBooks(int take = 0, OrderBy orderBy = OrderBy.Asc)
        {
            var retVal = _bookListRepos.GetAllBooks(take, orderBy).Select(x => (BookListViewModel)x).ToArray();
            return retVal;
        }
    }
}
