﻿using Data.Repos;
using System.Linq;
using LibData.Repos;
using Data.Entities;
using AdminLTE.Models;
using AdminLTE.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class RegisterController : Controller
    {
        IssuedBookRepository<IssuedBook> _issuedBookRepository;
        public RegisterController()
        {
            _issuedBookRepository = new IssuedBookRepository<IssuedBook>();
        }

        public IActionResult Index()
        {
            ViewBag.Readers = ReposExtensions.GetEntities<Reader>()
                .OrderBy(x => x.Name).Select(x => (ReaderViewModel)x).ToList();

            return View();
        }

        [HttpPost]
        public IActionResult GetContent(RegisterViewModel model)
        {
            if (model == null)
            {
                ViewBag.ErrorMessage = "Ошибка!";
                return PartialView("_RegisterContent");
            }

            if (model.ReaderId <= 0 && model.Barcode.IsEmpty() && model.OnlyOverdue == false
                && model.DateFrom == null && model.DateTo == null)
            {
                ViewBag.ErrorMessage = "Выбирите параметры поиска!";
                return PartialView("_RegisterContent");
            }

            var retVal = _issuedBookRepository
                .GetIssuedBooksByFilters(model.ReaderId, model.Barcode, model.OnlyOverdue, model.DateFrom, model.DateTo)
                .Select(x => (RegisterViewModel)x).ToArray();

            if (retVal.Length == 0)
            {
                ViewBag.ErrorMessage = "Ни чего не найдено.";
                return PartialView("_RegisterContent");
            }

            return PartialView("_RegisterContent", retVal);
        }
    }
}
