﻿using System.Collections.Generic;
using System.Linq;
using AdminLTE.ViewModels;
using Data.Entities;
using Data.Repos;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class TownsController : Controller
    {
        public IActionResult Index()
        {
            return View(GetAllTowns());
        }

        public IActionResult Edit(int id)
        {
            var town = (TownViewModel)ReposExtensions.GetEntity<Town>(x => x.Id == id);
            return PartialView("_Edit", town);
        }

        [HttpPost]
        public IActionResult Edit(TownViewModel model)
        {
            Town town;
            if (model.Id > 0)
            {
                town = ReposExtensions.GetEntity<Town>(x => x.Id == model.Id);
            }
            else
            {
                town = new Town();
            }

            town.Name = model.Name.Trim();
            var saveResult = ReposExtensions.AddOrUpdate(town);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!";
            }

            return PartialView("_TownsContent", GetAllTowns());
        }

        private IEnumerable<TownViewModel> GetAllTowns()
        {
            var retVal = ReposExtensions.GetEntities<Town>()
                .Select(x => (TownViewModel)x).OrderBy(x => x.Name).ToArray();
            return retVal;
        }
    }
}
