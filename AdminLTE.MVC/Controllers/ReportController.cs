﻿using System.Linq;
using LibData.Repos;
using Data.Entities;
using AdminLTE.Models;
using AdminLTE.ViewModels;
using AdminLTE.Models.Reports;
using Microsoft.AspNetCore.Mvc;
using Data.Repos;

namespace AdminLTE.Controllers
{
    public class ReportController : Controller
    {
        private BarcodesRepository<PrintBarcode> _barcodesRepository;

        public ReportController()
        {
            _barcodesRepository = new BarcodesRepository<PrintBarcode>();
        }

        [HttpGet]
        public IActionResult GetReport(int count, int bookId, bool print_doubles)
        {
            var isFirst = LibSessionExtensions.Get<string>(this, SessionVariables.FirstPrintReportInCurrentPage);
            ViewBag.FirstPrintReportInCurrentPage = isFirst;
            LibSessionExtensions.Set(this, SessionVariables.FirstPrintReportInCurrentPage, "0");

            var model = new ReportViewModel();

            if (count <= 0)
            {
                return PartialView("Index", model);
            }

            string bookTypeName = null;
            if (bookId > 0)
            {
                var bookType = ReposExtensions.GetEntity<BookType>(x => x.Id == bookId);
                if (bookType != null)
                {
                    bookTypeName = bookType.Name;
                }
            }

            var barcodes = _barcodesRepository.GetBarcodesPool(count);

            ReportBarcodes report = null;
            foreach (var barcode in barcodes)
            {
                if (report == null)
                {
                    report = new ReportBarcodes(barcode, bookTypeName);
                    report.CreateDocument();
                }
                else
                {
                    var nextRep = new ReportBarcodes(barcode, bookTypeName);
                    nextRep.CreateDocument();
                    report.Pages.AddRange(nextRep.Pages.ToList());
                }

                if (print_doubles)
                {
                    var double_rep = new ReportBarcodes(barcode, bookTypeName);
                    double_rep.CreateDocument();
                    report.Pages.AddRange(double_rep.Pages.ToList());
                }
            }

            model.ReportHeight = "400px";
            model.Report = report;

            return PartialView("Index", model);
        }
    }
}
