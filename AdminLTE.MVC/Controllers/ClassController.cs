﻿using System.Collections.Generic;
using System.Linq;
using AdminLTE.ViewModels;
using Data.Entities;
using Data.Repos;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class ClassController : Controller
    {
        public IActionResult Index()
        {
            return View(GetAllClasses());
        }

        public IActionResult Edit(int id)
        {
            var cl = (ClassViewModel)ReposExtensions.GetEntity<Class>(x => x.Id == id);
            return PartialView("_Edit", cl);
        }

        [HttpPost]
        public IActionResult Edit(ClassViewModel model)
        {
            Class cl;
            if (model.Id > 0)
            {
                cl = ReposExtensions.GetEntity<Class>(x => x.Id == model.Id);
            }
            else
            {
                cl = new Class();
            }

            cl.Name = model.Name.Trim();
            var saveResult = ReposExtensions.AddOrUpdate(cl);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!";
            }

            return PartialView("_ClassContent", GetAllClasses());
        }

        private IEnumerable<ClassViewModel> GetAllClasses()
        {
            var retVal = ReposExtensions.GetEntities<Class>()
                .Select(x => (ClassViewModel)x).OrderBy(x => x.Name).ToArray();
            return retVal;
        }
    }
}
