﻿using System.Collections.Generic;
using System.Linq;
using AdminLTE.ViewModels;
using Data.Entities;
using Data.Repos;
using Microsoft.AspNetCore.Mvc;

namespace AdminLTE.Controllers
{
    public class BookshelfController : Controller
    {
        public IActionResult Index()
        {
            return View(GetAllBookshelfs());
        }

        public IActionResult Edit(int id)
        {
            var bookShelf = (BookshelfViewModel)ReposExtensions.GetEntity<BookShelf>(x => x.Id == id);
            return PartialView("_Edit", bookShelf);
        }

        [HttpPost]
        public IActionResult Edit(BookshelfViewModel model)
        {
            BookShelf bookShelf;
            if (model.Id > 0)
            {
                bookShelf = ReposExtensions.GetEntity<BookShelf>(x => x.Id == model.Id);
            }
            else
            {
                bookShelf = new BookShelf();
            }

            bookShelf.Name = model.Name.Trim();
            var saveResult = ReposExtensions.AddOrUpdate(bookShelf);
            if (saveResult == false)
            {
                ViewBag.ErrorMessage = "Ошибка при сохранении!";
            }

            return PartialView("_BookshelContent", GetAllBookshelfs());
        }

        private IEnumerable<BookshelfViewModel> GetAllBookshelfs()
        {
            var retVal = ReposExtensions.GetEntities<BookShelf>()
                .Select(x => (BookshelfViewModel)x).OrderBy(x => x.Name).ToArray();
            return retVal;
        }
    }
}
