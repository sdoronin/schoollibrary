﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Data
{
    public class LibDbContext : IdentityDbContext<User>
    {
        public LibDbContext(DbContextOptions<LibDbContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var conStr = DbTools.GetConnetcionString();
            optionsBuilder.UseNpgsql(conStr);
            optionsBuilder.UseLazyLoadingProxies(true);
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<BookType> BookTypes { get; set; }
        public DbSet<BookGroup> BookGroups { get; set; }
        public DbSet<BookShelf> BookShelfs { get; set; }
        public DbSet<Reader> Readers { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<IssuedBook> IssuedBooks { get; set; }
        public DbSet<Class> Class { get; set; }
        public DbSet<PrintBarcode> PrintBarcodes { get; set; }
        public DbSet<Town> Towns { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookType>().HasIndex(x => x.Id).IsUnique();
            modelBuilder.Entity<BookShelf>().HasIndex(x => x.Id).IsUnique();
            modelBuilder.Entity<BookGroup>().HasIndex(x => x.Id).IsUnique();
            modelBuilder.Entity<Reader>().HasIndex(x => x.Id).IsUnique();
            modelBuilder.Entity<Author>().HasIndex(x => x.Id).IsUnique();
            modelBuilder.Entity<IssuedBook>().HasIndex(x => x.Id).IsUnique();
            modelBuilder.Entity<Class>().HasIndex(x => x.Id).IsUnique();
            modelBuilder.Entity<PrintBarcode>().HasIndex(x => x.Id).IsUnique();
            modelBuilder.Entity<Town>().HasIndex(x => x.Id).IsUnique();

            modelBuilder.Entity<BookType>().HasKey(x => x.Id);
            modelBuilder.Entity<BookShelf>().HasKey(x => x.Id);
            modelBuilder.Entity<BookGroup>().HasKey(x => x.Id);
            modelBuilder.Entity<Reader>().HasKey(x => x.Id);
            modelBuilder.Entity<Author>().HasKey(x => x.Id);
            modelBuilder.Entity<IssuedBook>().HasKey(x => x.Id);
            modelBuilder.Entity<Class>().HasKey(x => x.Id);
            modelBuilder.Entity<PrintBarcode>().HasKey(x => x.Id);
            modelBuilder.Entity<Town>().HasKey(x => x.Id);

            modelBuilder.Entity<BookType>()
                .HasOne(e => e.Author)
                .WithMany(e => e.BookTypes)
                .HasForeignKey(e => e.AuthorId);

            modelBuilder.Entity<BookType>()
                .HasOne(e => e.BookShelf)
                .WithMany(e => e.BookTypes)
                .HasForeignKey(e => e.BookShelfId);

            modelBuilder.Entity<IssuedBook>()
                .HasOne(e => e.Book)
                .WithMany(e => e.IssuedBooks)
                .HasForeignKey(e => e.BookId);

            modelBuilder.Entity<IssuedBook>()
                .HasOne(e => e.Reader)
                .WithMany(e => e.IssuedBooks)
                .HasForeignKey(e => e.ReaderId);

            modelBuilder.Entity<Reader>()
                .HasOne(e => e.Class)
                .WithMany(e => e.Readers)
                .HasForeignKey(e => e.ClassId);

            modelBuilder.Entity<Book>()
                .HasOne(e => e.BookType)
                .WithMany(e => e.Books)
                .HasForeignKey(e => e.BookTypeId);

            modelBuilder.Entity<BookType>()
                .HasOne(e => e.BookGroup)
                .WithMany(e => e.BookTypes)
                .HasForeignKey(e => e.BookGroupId);

            modelBuilder.Entity<BookType>()
                .HasOne(e => e.Town)
                .WithMany(e => e.BookTypes)
                .HasForeignKey(e => e.TownId);

            modelBuilder.Entity<IssuedBook>().Property(b => b.DayCount).HasDefaultValue(0);

            base.OnModelCreating(modelBuilder);
        }
    }
}
