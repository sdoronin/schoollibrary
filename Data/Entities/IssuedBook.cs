﻿using System;

namespace Data.Entities
{
    public class IssuedBook : Entity
    {
        public int BookId { get; set; }
        public int ReaderId { get; set; }
        public DateTime DateIssue { get; set; }
        public DateTime? DateReturn { get; set; }
        public bool IsReturned { get; set; }
        public int DayCount { get; set; }

        public virtual Book Book { get; set; }
        public virtual Reader Reader { get; set; }
    }
}
