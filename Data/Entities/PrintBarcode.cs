﻿using System;

namespace Data.Entities
{
    public class PrintBarcode : Entity
    {
        public int Barcode { get; set; }
        public int BarcodesCount { get; set; }
    }
}
