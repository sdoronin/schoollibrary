﻿using System.Collections.Generic;

namespace Data.Entities
{
    public class Author : Entity
    {
        public string Name { get; set; }

        public virtual ICollection<BookType> BookTypes { get; set; }
    }
}
