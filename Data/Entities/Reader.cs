﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class Reader : Entity
    {
        public string Name { get; set; }
        public string Pname { get; set; }
        public string Sname { get; set; }
        public int ClassId { get; set; }
        public string UniqueId { get; set; }
        
        public virtual Class Class { get; set; }
        public virtual ICollection<IssuedBook> IssuedBooks { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return this.Sname + " " + this.Name + " " + this.Pname;
            }
        }
    }
}
