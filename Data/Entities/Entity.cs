﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class Entity
    {
        public int Id { get; set; }

        [NotMapped]
        public bool IsNew
        {
            get
            {
                if (this.Id < 0)
                {
                    throw new Exception("Entity Id is not correct!");
                }

                return this.Id == 0;
            }
        }
    }
}
