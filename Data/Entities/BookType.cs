﻿using System.Collections.Generic;

namespace Data.Entities
{
    public class BookType : Entity
    {
        public int AuthorId { get; set; }
        public int BookGroupId { get; set; }
        public int Year { get; set; }
        public string ISBN { get; set; }
        public string Seria { get; set; }
        public int BookShelfId { get; set; }
        public string Name { get; set; }
        public string Publisher { get; set; }
        public int? TownId { get; set; }

        public virtual Author Author { get; set; }
        public virtual BookGroup BookGroup { get; set; }
        public virtual BookShelf BookShelf { get; set; }
        public virtual Town Town { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }
}
