﻿using System.Collections.Generic;

namespace Data.Entities
{
    public class Book : Entity
    {
        public string Barcode { get; set; }
        public int BookTypeId { get; set; }

        public virtual BookType BookType { get; set; }
        public virtual ICollection<IssuedBook> IssuedBooks { get; set; }
    }
}
