﻿using System.Collections.Generic;

namespace Data.Entities
{
    public class Class : Entity
    {
        public string Name { get; set; }

        public virtual ICollection<Reader> Readers { get; set; }
    }
}
