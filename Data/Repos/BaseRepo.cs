﻿using System;
using LogManager;
using System.Linq;
using Data.Entities;
using System.Linq.Expressions;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Data.Repos
{
    internal class BaseRepo<T> : IDisposable, IRepo<T> where T : Entity
    {
        private readonly DbSet<T> _table;

        public BaseRepo()
        {
            this.DbContext = DbTools.CreateDbContext();
            _table = DbContext.Set<T>();
        }

        internal protected LibDbContext DbContext { get; private set; }

        private bool SaveChanges()
        {
            var hasChandes = this.DbContext.ChangeTracker.Entries()
                .Any(e => e.State == EntityState.Added || e.State == EntityState.Modified || e.State == EntityState.Deleted);
            if (hasChandes == false)
            {
                return true;
            }

            string errorMessage = null;
            bool retVal;
            try
            {
                var res = this.DbContext.SaveChanges();
                retVal = res > 0;
            }
            catch (Exception ex)
            {
                retVal = false;
                errorMessage = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                Logger.Error("Error in BaseRepo: " + errorMessage);
            }
            finally
            {
                if (string.IsNullOrWhiteSpace(errorMessage) == false)
                {
                    throw new Exception(errorMessage);
                }
            }

            return retVal;
        }

        public void Dispose()
        {
            if (this.DbContext != null)
            {
                try
                {
                    this.DbContext.Dispose();
                    this.DbContext = null;
                }
                catch
                {
                    this.DbContext = null;
                }
            }
        }

        public bool AddEntities(IEnumerable<T> entities)
        {
            _table.AddRange(entities);
            return this.SaveChanges();
        }

        public bool AddEntity(T entity)
        {
            _table.Add(entity);
            return this.SaveChanges();
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> expression = null)
        {
            var query = _table.AsQueryable();
            if (expression != null)
            {
                query = query.Where(expression);
            }

            return query.ToArray();
        }

        public IQueryable<T> GetQuery()
        {
            return _table.AsQueryable();
        }

        public bool SaveEntities(IEnumerable<T> entities)
        {
            _table.UpdateRange(entities);
            return this.SaveChanges();
        }

        public bool SaveEntity(T entity)
        {
            _table.Update(entity);
            return this.SaveChanges();
        }

        public bool AddOrUpdateEntities(IEnumerable<T> entities)
        {
            var addedEntities = entities.Where(x => x.IsNew).ToArray();
            var savedEntities = entities.Where(x => x.IsNew == false).ToArray();
            bool hasChanged = false;

            if (addedEntities.Length > 0)
            {
                _table.AddRange(addedEntities);
                hasChanged = true;
            }

            if (savedEntities.Length > 0)
            {
                _table.UpdateRange(savedEntities);
                hasChanged = true;
            }

            bool retVal;
            if (hasChanged)
            {
                retVal = this.SaveChanges();
            }
            else
            {
                retVal = true;
            }

            return retVal;
        }
    }
}
