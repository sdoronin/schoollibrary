﻿using System;
using System.Linq;
using Data.Entities;
using System.Reflection;
using System.Linq.Expressions;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Data.Repos
{
    public static class ReposExtensions
    {
        public static T GetEntity<T>(Expression<Func<T, bool>> expression) where T : Entity
        {
            T retVal;
            using (var repo = new BaseRepo<T>())
            {
                var includes = GetIncludes(typeof(T));
                var query = repo.GetQuery();

                foreach (var inclide in includes)
                {
                    query = query.Include(inclide);
                }

                retVal = query.FirstOrDefault(expression);
            }

            return retVal;
        }

        public static IEnumerable<T> GetEntities<T>(Expression<Func<T, bool>> expression = null) where T : Entity
        {
            var includes = GetIncludes(typeof(T));

            T[] retVal;
            using (var repo = new BaseRepo<T>())
            {
                var query = repo.GetQuery();
                if (expression != null)
                {
                    query = query.Where(expression);
                }

                if (includes != null)
                {
                    foreach (var includ in includes)
                    {
                        query = query.Include(includ);
                    }
                }

                retVal = query.ToArray();
            }

            return retVal;
        }

        public static bool SaveEntity<T>(T entity) where T : Entity
        {
            bool saveResult;
            using (var repo = new BaseRepo<T>())
            {
                saveResult = repo.SaveEntity(entity);
            }

            return saveResult;
        }

        public static bool SaveEntities<T>(IEnumerable<T> entities) where T : Entity
        {
            bool saveResult;
            using (var repo = new BaseRepo<T>())
            {
                saveResult = repo.SaveEntities(entities);
            }

            return saveResult;
        }

        public static bool AddEntity<T>(T entity) where T : Entity
        {
            bool saveResult;
            using (var repo = new BaseRepo<T>())
            {
                saveResult = repo.AddEntity(entity);
            }

            return saveResult;
        }

        public static bool AddEntities<T>(IEnumerable<T> entities) where T : Entity
        {
            bool saveResult;
            using (var repo = new BaseRepo<T>())
            {
                saveResult = repo.SaveEntities(entities);
            }

            return saveResult;
        }

        public static bool AddOrUpdate<T>(T entity) where T : Entity
        {
            bool saveResult;
            using (var repo = new BaseRepo<T>())
            {
                if (entity.IsNew)
                {
                    saveResult = repo.AddEntity(entity);
                }
                else
                {
                    saveResult = repo.SaveEntity(entity);
                }
            }

            return saveResult;
        }

        public static bool AddOrUpdate<T>(IEnumerable<T> entities) where T : Entity
        {
            bool retVal;
            using (var repo = new BaseRepo<T>())
            {
                retVal = repo.AddOrUpdateEntities(entities);
            }

            return retVal;
        }

        private static IEnumerable<string> GetIncludes(Type entityType)
        {
            var retVal = new List<string>();
            var properties = entityType.GetProperties()
                .Where(x => x.GetGetMethod().IsVirtual && x.MemberType == MemberTypes.Property).ToArray();
            foreach (PropertyInfo prop in properties)
            {
                if (prop.PropertyType.FullName.Contains("ICollection") == false)
                {
                    retVal.Add(prop.PropertyType.Name);
                }
            }

            return retVal.ToArray();
        }
    }
}
