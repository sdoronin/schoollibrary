﻿using Data.Repos;
using System.Linq;
using Data.Entities;
using System.Collections.Generic;

namespace LibData.Repos
{
    public class BarcodesRepository<T> where T : PrintBarcode
    {
        public List<int> GetBarcodesPool(int count)
        {
            var retVal = new List<int>();
            int barcod;
            using (var repo = new BaseRepo<PrintBarcode>())
            {
                var lastBarcode = repo.DbContext.PrintBarcodes.OrderByDescending(x => x.Barcode).FirstOrDefault();
                if (lastBarcode == null)
                {
                    lastBarcode = new PrintBarcode
                    {
                        Barcode = 1,
                        BarcodesCount = count
                    };

                    repo.DbContext.PrintBarcodes.Add(lastBarcode);
                    repo.DbContext.SaveChanges();

                    barcod = 1;
                    for (int i = 0; i < count; i++)
                    {
                        retVal.Add(barcod);
                        barcod++;
                    }
                }
                else
                {
                    barcod = lastBarcode.Barcode + lastBarcode.BarcodesCount + 1;
                    var newBarcod = new PrintBarcode
                    {
                        Barcode = barcod,
                        BarcodesCount = count
                    };

                    repo.DbContext.PrintBarcodes.Add(newBarcod);
                    repo.DbContext.SaveChanges();

                    for (int i = 0; i < count; i++)
                    {
                        retVal.Add(barcod);
                        barcod++;
                    }
                }
            }

            return retVal;
        }
    }
}
