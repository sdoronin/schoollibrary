﻿using Data.Repos;
using System.Linq;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using static LibData.Enums;
using System.Collections.Generic;
using System;

namespace LibData.Repos
{
    public class BookListRepository<T> where T : Book
    {
        public Book[] GetAllBooks(int take = 0, OrderBy orderBy = OrderBy.Not)
        {
            Book[] retVal;
            using (var repo = new BaseRepo<Book>())
            {
                var query = repo.GetQuery()
                    .Include(x => x.BookType)
                    .Include(x => x.BookType.Author)
                    .Include(x => x.BookType.BookGroup)
                    .Include(x => x.BookType.BookShelf).AsQueryable();
                if (orderBy == OrderBy.Asc)
                {
                    query = query.OrderBy(x => x.Id);
                }
                else if (orderBy == OrderBy.Desc)
                {
                    query = query.OrderByDescending(x => x.Id);
                }

                if (take > 0)
                {
                    query = query.Take(take);
                }

                retVal = query.ToArray();
            }

            return retVal;
        }

        public Book[] GetBooksByBarcodes(string[] barcodesArr)
        {
            if (barcodesArr == null || barcodesArr.Length == 0)
            {
                return new Book[] { };
            }

            var barcodes = new List<string>();
            foreach (var barcod in barcodesArr)
            {
                if (barcod.Contains(":"))
                {
                    var arr = barcod.Split(new string[] { ":" }, StringSplitOptions.None);
                    barcodes.Add(arr[0]);
                }
                else
                {
                    barcodes.Add(barcod);
                }
            }

            Book[] retVal;
            using (var repo = new BaseRepo<Book>())
            {
                var query = repo.GetQuery()
                    .Include(x => x.BookType)
                    .Include(x => x.BookType.Author)
                    .Include(x => x.BookType.BookGroup)
                    .Include(x => x.BookType.BookShelf).AsQueryable();

                query = query.Where(x => barcodes.Contains(x.Barcode));

                retVal = query.ToArray();
            }

            return retVal;
        }

        public Book GetBook(int id)
        {
            if (id <= 0)
            {
                return null;
            }

            Book retVal;
            using (var repo = new BaseRepo<Book>())
            {
                var query = repo.GetQuery()
                    .Include(x => x.BookType)
                    .Include(x => x.BookType.Author)
                    .Include(x => x.BookType.BookGroup)
                    .Include(x => x.BookType.BookShelf);
                retVal = query.FirstOrDefault(x => x.Id == id);
            }

            return retVal;
        }

        public Book GetBook(string barcode)
        {
            if (string.IsNullOrWhiteSpace(barcode))
            {
                return null;
            }

            barcode = barcode.Trim();

            Book retVal;
            using (var repo = new BaseRepo<Book>())
            {
                var query = repo.GetQuery()
                    .Include(x => x.BookType)
                    .Include(x => x.BookType.Author)
                    .Include(x => x.BookType.BookGroup)
                    .Include(x => x.BookType.BookShelf);
                retVal = query.FirstOrDefault(x => x.Barcode == barcode);
            }

            return retVal;
        }

        public string HasBook(Book book)
        {
            string retVal = null;
            using (var repo = new BaseRepo<Book>())
            {
                var b = repo.GetQuery().FirstOrDefault(x => x.Barcode == book.Barcode && x.Id != book.Id);
                if (b != null)
                {
                    retVal = "Книга с таким ШК (" + book.Barcode + "), существует: " + b.BookType.Name;
                }
            }

            return retVal;
        }

        public string AddNewBook(Book book)
        {
            string retVal = null;
            using (var repo = new BaseRepo<Book>())
            {
                var item = repo.GetQuery().FirstOrDefault(x => x.Barcode == book.Barcode);
                if (item != null)
                {
                    retVal = "Книга с таким ШК (" + book.Barcode + "), существует: " + item.BookType.Name;
                }
                else
                {
                    var saveResult = repo.AddEntity(book);
                    if (saveResult == false)
                    {
                        retVal = "Ошибка при сохранении книги!";
                    }
                }
            }

            return retVal;
        }

        public string CheckBookFromReaderForLeading(string barcode, int readerId, string[] barcodes = null)
        {
            string retVal = null;
            using (var repo = new BaseRepo<Book>())
            {
                var book = repo.DbContext.Books.FirstOrDefault(x => x.Barcode == barcode);
                if (book == null)
                {
                    retVal = "Не найдена книга по штрихкоду";
                    return retVal;
                }

                var bookTypeId = book.BookTypeId;
                var hasIssue = repo.DbContext.IssuedBooks.FirstOrDefault(x =>
                        x.Book.BookTypeId == bookTypeId
                        && x.IsReturned == false);

                if (hasIssue != null && hasIssue.ReaderId != readerId)
                {
                    retVal = "Данная книга на руках у : " + hasIssue.Reader.FullName;
                    return retVal;
                }

                if (hasIssue != null && hasIssue.ReaderId == readerId)
                {
                    retVal = "У данного читателя уже есть такая книга со штрихкодом: " + hasIssue.Book.Barcode;
                    return retVal;
                }

                if (barcodes != null && barcodes.Length > 0)
                {
                    var items = repo.DbContext.Books
                        .Where(x => barcodes.Contains(x.Barcode) && x.Barcode != barcode).ToList();
                    var item = items.FirstOrDefault(x => x.BookTypeId == book.BookTypeId);
                    if (item != null)
                    {
                        retVal = "Такую книгу только что выдали: " + item.Barcode;
                    }
                }
            }

            return retVal;
        }

        public string CheckBookFromReaderForReturn(string barcode, int readerId)
        {
            string retVal = null;
            using (var repo = new BaseRepo<Book>())
            {
                var book = repo.DbContext.Books.FirstOrDefault(x => x.Barcode == barcode);
                if (book == null)
                {
                    retVal = "Не найдена книга по штрихкоду";
                    return retVal;
                }

                var bookTypeId = book.BookTypeId;
                var hasIssue = repo.DbContext.IssuedBooks.FirstOrDefault(x =>
                        x.ReaderId == readerId
                        && x.Book.BookTypeId == bookTypeId
                        && x.IsReturned == false);

                if (hasIssue == null)
                {
                    retVal = "У данного читателя нет книги";
                    return retVal;
                }

                if (hasIssue.IsReturned)
                {
                    retVal = "Книга со штрихкодом "+ hasIssue.Book.Barcode + " уже возвращена";
                    return retVal;
                }
            }

            return retVal;
        }
    }
}
