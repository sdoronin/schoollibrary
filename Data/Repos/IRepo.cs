﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Data.Repos
{
    internal interface IRepo<T>
    {
        bool AddEntity(T entity);
        bool AddEntities(IEnumerable<T> entities);
        bool SaveEntity(T entity);
        bool SaveEntities(IEnumerable<T> entities);
        IQueryable<T> GetQuery();
        IEnumerable<T> GetAll(Expression<Func<T, bool>> expression = null);
    }
}
