﻿using System;
using Data.Repos;
using System.Linq;
using Data.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace LibData.Repos
{
    public class IssuedBookRepository<T> where T : IssuedBook
    {
        public bool ReturnBooks(Book[] books, int readerId)
        {
            bool retVal = false;
            if (books == null || books.Length == 0)
            {
                return retVal;
            }

            if (readerId <= 0)
            {
                return retVal;
            }

            var dtNow = DateTime.Now;
            var issues = new List<IssuedBook>();
            var bookRepo = new BookListRepository<Book>();
            using (var issueRepo = new BaseRepo<IssuedBook>())
            {
                foreach (var book in books)
                {
                    var hasCheck = bookRepo.CheckBookFromReaderForReturn(book.Barcode, readerId);
                    if (string.IsNullOrWhiteSpace(hasCheck) == false)
                    {
                        continue;
                    }

                    var bookTypeId = book.BookTypeId;
                    var issue = issueRepo.DbContext.IssuedBooks.FirstOrDefault(x =>
                        x.ReaderId == readerId
                        && x.Book.BookTypeId == bookTypeId
                        && x.IsReturned == false);
                    if (issue == null)
                    {
                        continue;
                    }

                    issue.IsReturned = true;
                    issue.DateReturn = dtNow;
                    issues.Add(issue);
                }

                issueRepo.DbContext.IssuedBooks.UpdateRange(issues);
                retVal = issueRepo.DbContext.SaveChanges() > 0;
            }

            return retVal;
        }

        public bool CreateIssuesByBooks(Book[] books, int readerId, string[] barcodes)
        {
            bool retVal = false;
            if (books == null || books.Length == 0)
            {
                return retVal;
            }

            if (readerId <= 0)
            {
                return retVal;
            }

            var dtNow = DateTime.Now;
            var bookRepo = new BookListRepository<Book>();
            var issues = new List<IssuedBook>();
            using (var issueRepo = new BaseRepo<IssuedBook>())
            {
                foreach (var barcode in barcodes)
                {
                    string barcod;
                    int dayCount = 0;
                    if (barcode.Contains(":"))
                    {
                        var arr = barcode.Split(new string[] { ":" }, StringSplitOptions.None);
                        barcod = arr[0];
                        if (arr.Length == 2 && string.IsNullOrWhiteSpace(arr[1]) == false)
                        {
                            dayCount = Convert.ToInt32(arr[1].Trim());
                        }
                    }
                    else
                    {
                        barcod = barcode;
                    }

                    var book = books.FirstOrDefault(x => x.Barcode == barcod);
                    if (book == null)
                    {
                        continue;
                    }

                    var hasCheck = bookRepo.CheckBookFromReaderForLeading(book.Barcode, readerId);
                    if (string.IsNullOrWhiteSpace(hasCheck) == false)
                    {
                        continue;
                    }

                    var issue = new IssuedBook
                    {
                        BookId = book.Id,
                        IsReturned = false,
                        ReaderId = readerId,
                        DateIssue = dtNow,
                        DateReturn = null,
                        DayCount = dayCount
                    };

                    issues.Add(issue);
                }

                issueRepo.DbContext.IssuedBooks.AddRange(issues);
                retVal = issueRepo.DbContext.SaveChanges() > 0;
            }

            return retVal;
        }

        public IssuedBook[] GetIssuedBooksByFilters(int readerId, string barcode, bool onlyOverdue,
            DateTime? dt_from, DateTime? dt_to)
        {
            IssuedBook[] retVal = null;
            using (var repo = new BaseRepo<IssuedBook>())
            {
                var query = repo.DbContext.IssuedBooks
                    .Include(x => x.Reader)
                    .Include(x => x.Book)
                    .Include(x => x.Book.BookType)
                    .Include(x => x.Book.BookType.Author)
                    .Include(x => x.Book.BookType.BookGroup)
                    .Include(x => x.Book.BookType.BookShelf)
                    .AsQueryable();

                if (string.IsNullOrWhiteSpace(barcode) == false)
                {
                    barcode = barcode.Trim();
                    query = query.Where(x => x.Book.Barcode == barcode && x.IsReturned == false);
                }
                else if (readerId > 0)
                {
                    query = query.Where(x => x.ReaderId == readerId && x.IsReturned == false);
                }
                else
                {
                    retVal = new IssuedBook[] { };
                }

                if (dt_from != null && dt_from != null && dt_to < dt_from)
                {
                    return new IssuedBook[] { };
                }

                if (dt_from != null)
                {
                    dt_from = new DateTime(dt_from.Value.Year, dt_from.Value.Month, dt_from.Value.Day, 0, 0, 0);
                    query = query.Where(x => x.DateIssue >= dt_from);
                }

                if (dt_to != null)
                {
                    dt_to = new DateTime(dt_to.Value.Year, dt_to.Value.Month, dt_to.Value.Day, 23, 59, 59);
                    query = query.Where(x => x.DateIssue <= dt_to);
                }

                retVal = query.ToArray();
                if (onlyOverdue)
                {
                    retVal = retVal
                        .Where(x => x.DayCount > 0
                            && x.DateIssue.AddDays(x.DayCount) < DateTime.Now)
                        .ToArray();
                }
            }

            return retVal;
        }
    }
}
