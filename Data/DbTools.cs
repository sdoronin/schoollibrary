﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace Data
{
    public static class DbTools
    {
        public static string GetConnetcionString()
        {
            string conStr;
#if (DEBUG)
            conStr = @"Server=192.168.3.53;Port=5432;Database=school_lib;User Id=lis;Password=Stella15Via43;";
#else
            conStr = @"Server=localhost;Port=5432;Database=school_lib;User Id=postgres;Password=Stella15Via43;";
#endif
            return conStr;
        }

        public static LibDbContext CreateDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<LibDbContext>();
            var connection = GetConnetcionString();
            optionsBuilder.UseNpgsql(connection, o => o.EnableRetryOnFailure())
                .ConfigureWarnings(w => w.Throw(RelationalEventId.QueryClientEvaluationWarning));
            return new LibDbContext(optionsBuilder.Options);
        }
    }
}
